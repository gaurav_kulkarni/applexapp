package com.app.applexapp.activity.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.app.applexapp.R;
import com.app.applexapp.activity.CurrentUser;
import com.app.applexapp.activity.DashboardActivity;
import com.app.applexapp.model.User;
import com.app.applexapp.network.RetrofitClient;
import com.app.applexapp.network.ServiceApi;
import com.app.applexapp.util.AlertDialog;
import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private EditText usernameText, passwordText;
    private LinearLayout llWarning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameText = findViewById(R.id.edtUsername);
        passwordText = findViewById(R.id.edtPassword);
        llWarning = findViewById(R.id.llWarning);

        getSupportActionBar().hide();
    }

    public void login(View view) {

        if (!validate()) {
            return;
        }
        signIn(usernameText.getText().toString(), passwordText.getText().toString());
    }

    public boolean validate() {

        boolean valid = true;

        String username = usernameText.getText().toString().trim();
        String password = passwordText.getText().toString().trim();

        if (username.isEmpty()) {
            llWarning.setVisibility(View.VISIBLE);
            valid = false;
        } else {
//            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 6) {
            llWarning.setVisibility(View.VISIBLE);
            valid = false;
        } else {
//            _passwordText.setError(null);
        }
        return valid;
    }

    void signIn(String username, String Pass) {

        final ServiceApi serviceApi = RetrofitClient.getClient().create(ServiceApi.class);
        final Call<User> aboutUs = serviceApi.UserLogin(username, Pass);
        aboutUs.enqueue(new Callback<User>() {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                User user = response.body();
                if (user != null && !user.getRESPONSESTATUS().equals("0")) {
                    onLoginSuccess(user);

                } else {
                    Toast.makeText(LoginActivity.this, "" + user.getRESPONSEMSG(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

                onLoginFailure();
            }
        });
    }

    public void onLoginSuccess(final User user) {

        AlertDialog mAlertDialog = new AlertDialog(this);
        mAlertDialog.showDialog("Logged in Successfully", "After active hours your account will auto logout");
        mAlertDialog.setListener(new AlertDialog.IAlertListener() {
            @Override
            public void onClick(Object o) {
                CallActivity(user);
                CurrentUser.getInstance().setCurrentUser(user);
            }
        });

    }

    private void CallActivity(User user) {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("user", user);
        startActivity(intent);
        finish();
    }

    public void onLoginFailure() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
    }

    public void launchForgotPasswordActivity(View view) {

        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    public void createSnackbar(Context context, View view) {

        Snackbar snackbar = Snackbar.make(view, null, Snackbar.LENGTH_LONG);
        Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbar.getView();
        snackbarLayout.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View snackView = layoutInflater.inflate(R.layout.snackbar_success, null);
        //If the view is not covering the whole snackbar layout, add this line
        snackbarLayout.setPadding(0, 0, 0, 0);
        //Add the view to the Snackbar's layout
        snackbarLayout.addView(snackView, 0);
        //Show the Snackbar
        snackbar.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}

package com.app.applexapp.fragmnetlistener;

import com.app.applexapp.model.BatchDetail;

public interface IFragmentListener {

    void sendBatchDetails(BatchDetail batchDetail);
}

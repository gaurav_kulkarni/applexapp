package com.app.applexapp.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailScan {

    @SerializedName("RESPONSE_STATUS")
    @Expose
    private String rESPONSESTATUS;
    @SerializedName("RESPONSE_MSG")
    @Expose
    private String rESPONSEMSG;
    @SerializedName("Asset_Details")
    @Expose
    private AssetDetails assetDetails;

    public String getRESPONSESTATUS() {
        return rESPONSESTATUS;
    }

    public void setRESPONSESTATUS(String rESPONSESTATUS) {
        this.rESPONSESTATUS = rESPONSESTATUS;
    }

    public String getRESPONSEMSG() {
        return rESPONSEMSG;
    }

    public void setRESPONSEMSG(String rESPONSEMSG) {
        this.rESPONSEMSG = rESPONSEMSG;
    }

    public AssetDetails getAssetDetails() {
        return assetDetails;
    }

    public void setAssetDetails(AssetDetails assetDetails) {
        this.assetDetails = assetDetails;
    }

    public class AssetDetails {

        @SerializedName("asset_id")
        @Expose
        private String assetId;
        @SerializedName("asset_number")
        @Expose
        private String assetNumber;
        @SerializedName("asset_class")
        @Expose
        private String assetClass;
        @SerializedName("asset_description")
        @Expose
        private String assetDescription;
        @SerializedName("cap_date")
        @Expose
        private String capDate;
        @SerializedName("book_qty")
        @Expose
        private String bookQty;
        @SerializedName("gross_block")
        @Expose
        private String grossBlock;
        @SerializedName("cost_center")
        @Expose
        private String costCenter;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("sub_location")
        @Expose
        private String sub_location;
        @SerializedName("responsible_department")
        @Expose
        private String responsible_department;

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getSub_location() {
            return sub_location;
        }

        public void setSub_location(String sub_location) {
            this.sub_location = sub_location;
        }

        public String getResponsible_department() {
            return responsible_department;
        }

        public void setResponsible_department(String responsible_department) {
            this.responsible_department = responsible_department;
        }

        public String getAssetId() {
            return assetId;
        }

        public void setAssetId(String assetId) {
            this.assetId = assetId;
        }

        public String getAssetNumber() {
            return assetNumber;
        }

        public void setAssetNumber(String assetNumber) {
            this.assetNumber = assetNumber;
        }

        public String getAssetClass() {
            return assetClass;
        }

        public void setAssetClass(String assetClass) {
            this.assetClass = assetClass;
        }

        public String getAssetDescription() {
            return assetDescription;
        }

        public void setAssetDescription(String assetDescription) {
            this.assetDescription = assetDescription;
        }

        public String getCapDate() {
            return capDate;
        }

        public void setCapDate(String capDate) {
            this.capDate = capDate;
        }

        public String getBookQty() {
            return bookQty;
        }

        public void setBookQty(String bookQty) {
            this.bookQty = bookQty;
        }

        public String getGrossBlock() {
            return grossBlock;
        }

        public void setGrossBlock(String grossBlock) {
            this.grossBlock = grossBlock;
        }

        public String getCostCenter() {
            return costCenter;
        }

        public void setCostCenter(String costCenter) {
            this.costCenter = costCenter;
        }

    }
}
package com.app.applexapp.fragment;

import com.app.applexapp.model.BatchDetail;

public class SetCurrentBatch {

    private SetCurrentBatch(){

    }

    private BatchDetail mBatchDetail;
    private static SetCurrentBatch mSetCurrentBatch=null;
    public static SetCurrentBatch getInstance(){

        if (mSetCurrentBatch==null){
            mSetCurrentBatch=new SetCurrentBatch();
        }
        return mSetCurrentBatch;
    }

    public void setCurrentBatchDetails(BatchDetail batchDetails){
        mBatchDetail=batchDetails;
    }

    public BatchDetail getCurrentBatch(){
        return mBatchDetail;
    }


}

package com.app.applexapp.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.applexapp.R;
import com.app.applexapp.fragment.AssetListFragment;
import com.app.applexapp.fragment.DashBoardFragment;
import com.app.applexapp.fragment.SetBatchFragment;
import com.app.applexapp.fragment.SetCurrentBatch;
import com.app.applexapp.fragmnetlistener.IActivityListener;
import com.app.applexapp.fragmnetlistener.IFragmentListener;
import com.app.applexapp.model.BatchDetail;
import com.app.applexapp.model.User;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class DashboardActivity extends AppCompatActivity implements IFragmentListener {

    private Toolbar mToolbar;
    private User user;
    private IActivityListener mIActivityListener;
    String code;

    public void registerActivityListener(IActivityListener mIActivityListener) {
        this.mIActivityListener = mIActivityListener;
    }

    @Override
    public void sendBatchDetails(BatchDetail batchDetail) {
        mIActivityListener.getBatchDetails(batchDetail);
    }

    public interface OnBackPress {
        boolean onHardKeyBackPress();
    }

    private OnBackPress mOnBackPress;

    public void registerEventListener(OnBackPress mOnBackPress) {
        this.mOnBackPress = mOnBackPress;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_dashbord);

        mToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("Dashboard");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_button);

        RelativeLayout setBatch = findViewById(R.id.ivSetBatch);
        LinearLayout mScan = findViewById(R.id.ivCameraIcon);

        user = getIntent().getExtras().getParcelable("user");

        if (user.getRESPONSESTATUS().equals("4")) {

            showExitPOP(user.getRESPONSEMSG(), false);
        } else if ((user.getRESPONSESTATUS().equals("5"))) {
            showExitPOP(user.getRESPONSEMSG(), false);
        }

        mScan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (SetCurrentBatch.getInstance().getCurrentBatch() != null) {
                    new IntentIntegrator(DashboardActivity.this).setCaptureActivity(ScannerActivity.class).initiateScan();
                } else {
                    Toast.makeText(DashboardActivity.this, "Please Set current Batch", Toast.LENGTH_SHORT).show();
                }
            }
        });


        addFrag(new DashBoardFragment(), DashBoardFragment.class.getName(), false);
        callSetBatchFragment();
        setBatch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                callSetBatchFragment();
            }
        });

        RelativeLayout dashboard = findViewById(R.id.ivDashBord);
        dashboard.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mToolbar.setVisibility(View.VISIBLE);
                getSupportFragmentManager().popBackStack();
                addFrag(new DashBoardFragment(), DashBoardFragment.class.getName(), false);
            }
        });
        RelativeLayout assetList = findViewById(R.id.ivBatchList);

        assetList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                callAssetListFragment();
            }
        });
    }

    private void callSetBatchFragment() {
        getSupportFragmentManager().popBackStack();
        SetBatchFragment frag = new SetBatchFragment();
        Bundle b = new Bundle();
        b.putParcelable("user", user);
        frag.setArguments(b);
        addFrag(frag, SetBatchFragment.class.getName(), true);
        mToolbar.setVisibility(View.GONE);
    }

    public void callAssetListFragment() {
        mToolbar.setVisibility(View.GONE);
        getSupportFragmentManager().popBackStack();
        addFrag(new AssetListFragment(), AssetListFragment.class.getName(), true);
    }

    public void showToolBar() {
        mToolbar.setVisibility(View.VISIBLE);
    }

    private Fragment getFrag(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    public void addFrag(Fragment fragment, String Tag, boolean isAddTobackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.lay_dashbordView, fragment, Tag);
        if (isAddTobackStack)
            transaction.addToBackStack(Tag);
        transaction.commit();
    }

    public void addFrag(Fragment fragment, String Tag) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.fullScreenView, fragment, Tag);
        transaction.addToBackStack(Tag);
        transaction.commit();
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_profile:
                Intent i = new Intent(DashboardActivity.this, ProfileActivity.class);
                startActivity(i);
                return true;
            case R.id.action_share_app:
                Toast.makeText(getApplicationContext(), "Item 2 Selected", Toast.LENGTH_LONG).show();
                return true;
            case R.id.action_about_us:
                Intent intent = new Intent(this, AboutUsActivity.class);
                startActivity(intent);
                return true;
            case android.R.id.home:
                showExitPOP("", true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (mOnBackPress != null) {
            mOnBackPress.onHardKeyBackPress();
        } else {
            mToolbar.setVisibility(View.VISIBLE);
        }
        if (getSupportFragmentManager().getBackStackEntryCount() < 1)
            showExitPOP("", true);
        else {
            super.onBackPressed();
        }
    }

    private void ExitApp() {
        CurrentUser.getInstance().setCurrentUser(null);
        SetCurrentBatch.getInstance().setCurrentBatchDetails(null);
        this.finish();
    }

    private void clearStack(String name) {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(name, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //We will get scan results here
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        //check for null
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Scan Cancelled", Toast.LENGTH_LONG).show();
                //callView(2);
            } else {
                //callView(1);
                //show dialogue with result
                Log.d("Scan result", "onActivityResult: " + result.getContents());
                try {
                    String[] scanCodeArray = result.getContents().split(" | ");
                    code = scanCodeArray[0]/* + "-" + scanCodeArray[1] + "-" + scanCodeArray[2]*/;
                    Log.d("Code", "" + code);
                    callView(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // 7 1100 VW14000036-0-0 | Computers | SAP No VW14000036 | Lasear jet multi functional printer | Qty 1
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    void callView(int event) {
        switch (event) {
            case 1:
                viewHolder();
                break;
            case 2:
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        callAssetListFragment();
                    }
                }, 10);
                break;
        }
    }

    void viewHolder() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // mToolbar.setVisibility(View.GONE);
                        callDetailActivity();
                    }
                });
            }
        }, 10);
    }

    void callDetailActivity() {
        Intent i = new Intent(this, DetailsViewActivity.class);
        i.putExtra("user", user);
        i.putExtra("Code", code);
        startActivity(i);
    }

    public Dialog showExitPOP(String text, boolean isShowTwoButton) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.lay_exit_dialog);
        TextView test = dialog.findViewById(R.id.textmsg);

        if (!isShowTwoButton) {
            test.setText(text);
        }

        Button btnYes = dialog.findViewById(R.id.btnYes);
        Button btnNo = dialog.findViewById(R.id.btnNo);

        if (isShowTwoButton) {
            btnNo.setVisibility(View.VISIBLE);
            btnNo.setText("No");
            btnYes.setVisibility(View.VISIBLE);
            btnYes.setText("Yes");
        } else {
            btnNo.setVisibility(View.GONE);
            btnYes.setVisibility(View.VISIBLE);
            btnYes.setText("Ok");
        }
        btnNo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ExitApp();
                dialog.dismiss();
            }
        });

        dialog.show();
        return dialog;
    }
}

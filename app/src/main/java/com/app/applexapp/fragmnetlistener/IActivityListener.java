package com.app.applexapp.fragmnetlistener;

import com.app.applexapp.model.BatchDetail;

public interface IActivityListener {
    void getBatchDetails(BatchDetail batchDetail);
}

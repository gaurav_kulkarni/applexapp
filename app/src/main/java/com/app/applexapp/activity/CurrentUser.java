package com.app.applexapp.activity;

import com.app.applexapp.model.User;

public class CurrentUser {

    private User user;

    private static CurrentUser mCurrentUser;

    private CurrentUser() {

    }

    public static CurrentUser getInstance() {
        if (mCurrentUser == null) {
            synchronized (CurrentUser.class) {
                mCurrentUser = new CurrentUser();
            }
        }
        return mCurrentUser;
    }

    public User getUser() {
        return user;
    }

    public void setCurrentUser(User mUser) {
        this.user = mUser;
    }
}

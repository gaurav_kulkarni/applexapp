package com.app.applexapp.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.applexapp.R;
import com.app.applexapp.fragment.DetailsViewModel;
import com.app.applexapp.fragment.SetCurrentBatch;
import com.app.applexapp.model.BatchDetail;
import com.app.applexapp.model.DetailScan;
import com.app.applexapp.model.Reson;
import com.app.applexapp.model.SendRespose;
import com.app.applexapp.model.User;

import java.util.ArrayList;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class DetailsViewActivity extends AppCompatActivity {

    private DetailsViewModel mViewModel;

    private int EVENT = 0;
    private String code;
    private DetailScan.AssetDetails assetDetails;
    private BatchDetail batchDetail;
    private User mUser;
    private Spinner reason;
    private DetailsViewActivity mDetailsViewActivity;
    private Spinner SplReson;
    private ArrayList<Reson.ReasonDetail> detailsReson;
    private boolean isFromAsset, isManulScan;
    private EditText inUse, notInUse, notFound;
    private Spinner condition;
    private EditText remark;
    private String RegionScan = "";
    TextView assetNo;
    TextView tvAssetClassDescription;
    TextView tvassetDescription;
    TextView tvDateCapitalization;
    TextView tvBookQt;
    TextView tvGvalue;
    TextView tvCostCenter;
    LinearLayout assetView;
    TextView lastScanStatus;
    TextView seleCtcodntitle;
    TextView tvResponsibledepartment;
    TextView tvLocation;
    TextView tvSubLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_view);

        mViewModel = ViewModelProviders.of(this).get(DetailsViewModel.class);
        assetNo = findViewById(R.id.tvAssetNumber);
        tvAssetClassDescription = findViewById(R.id.tvAssetClassDescription);
        tvassetDescription = findViewById(R.id.tvassetDescription);
        tvDateCapitalization = findViewById(R.id.tvDateCapitalization);
        tvBookQt = findViewById(R.id.tvBookQt);
        tvGvalue = findViewById(R.id.tvGvalue);
        tvCostCenter = findViewById(R.id.tvCostCenter);
        lastScanStatus = findViewById(R.id.lastScanStatus);
        condition = findViewById(R.id.spConditionBase);
        remark = findViewById(R.id.EdRemark);

        inUse = findViewById(R.id.edInUse);
        notInUse = findViewById(R.id.edNotInUse);
        notFound = findViewById(R.id.edNotFound);

        //reason = findViewById(R.id.spSelctReson);
        assetView = findViewById(R.id.fromAsset);

        seleCtcodntitle = findViewById(R.id.tvSelectCondn);
        tvResponsibledepartment=findViewById(R.id.tvResponsibledepartment);
        tvLocation=findViewById(R.id.tvLocation);
        tvSubLocation=findViewById(R.id.tvSubLocation);

        mDetailsViewActivity = this;
        ImageView iv = findViewById(R.id.back);
        Bundle bundle = getIntent().getExtras();
        condition.getSelectedItem();
        showPOP = true;
        mUser = CurrentUser.getInstance().getUser();
        if (mUser != null) {
            Log.d("User", "onCreate: " + mUser.getRESPONSEMSG());
        }
        Intent intent = getIntent();
        if (intent != null) {

            code = intent.getStringExtra("Code");
            isFromAsset = intent.getBooleanExtra("isFromAsset", false);
            isManulScan = intent.getBooleanExtra("isManual", false);
            RegionScan = intent.getStringExtra("r");

        }

        ImageView img = findViewById(R.id.back);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getDetails("0");

        final ArrayList<String> resonString = new ArrayList<>();
        detailsReson = new ArrayList<>();
        final ArrayAdapter<String> resonAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, resonString);
        SplReson = findViewById(R.id.spSelctReson);
        SplReson.setPrompt("Select Reason");
        resonString.add("-Select Reason-");
        SplReson.setAdapter(resonAdapter);

        mViewModel.getAllResons().observe(this, new Observer<Reson>() {
            @Override
            public void onChanged(Reson reson) {

                if (reson.getReasonDetails() != null) {
                    for (int i = 0; i < reson.getReasonDetails().size(); i++) {
                        detailsReson.add(reson.getReasonDetails().get(i));
                        resonString.add(reson.getReasonDetails().get(i).getReasonTitle());
                        resonAdapter.notifyDataSetChanged();
                    }

                }
            }
        });


        Button b = findViewById(R.id.btSubmit);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubmmitAssetApi();
            }
        });

    }

    private boolean showPOP = false;
    private boolean isOtherBatch=false;

    private void getDetails(String verfy) {



        if (isFromAsset || isManulScan) {
            seleCtcodntitle.setVisibility(View.GONE);
            condition.setVisibility(View.GONE);
            if (isFromAsset)
                assetView.setVisibility(View.VISIBLE);

            batchDetail = SetCurrentBatch.getInstance().getCurrentBatch();
            String data = "";
            String unique_code = "";

            if (isFromAsset) {
                data = data + code;
            } else if (isManulScan) {
                unique_code = unique_code + code;
            }
            String other_batch="";
            if (isOtherBatch){

                other_batch +="1";
            }else {
                other_batch+="0";
            }

            mViewModel.getAssetDetails(batchDetail.getBatchId(), batchDetail.getProjectId(), unique_code, other_batch, verfy, "", data).observe(this, new Observer<DetailScan>() {
                @Override
                public void onChanged(DetailScan detailScan) {
                    Toast.makeText(mDetailsViewActivity, "" + detailScan.getRESPONSEMSG(), Toast.LENGTH_SHORT).show();
                    assetDetails = detailScan.getAssetDetails();
                    Dialog dialog = null;
                    if (detailScan.getRESPONSESTATUS().equals("3")) {
                        if (showPOP) {
                            isOtherBatch=false;
                            showPOP = false;
                            String[] dates = detailScan.getRESPONSEMSG().split(" ");
                            lastScanStatus.setText(dates[(dates.length - 1)]);
                            dialog = showCodeEnterDialog(detailScan.getRESPONSEMSG(),true);
                        }

                    }else if(detailScan.getRESPONSESTATUS().equals("2")){

                        if (showPOP) {
                            showPOP = false;
                            isOtherBatch=true;

                            dialog = showCodeEnterDialog(detailScan.getRESPONSEMSG(),false);
                        }

                    }
                    if (assetDetails != null) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        assetNo.setText("" + assetDetails.getAssetNumber());
                        tvAssetClassDescription.setText("" + assetDetails.getAssetClass());
                        tvassetDescription.setText(assetDetails.getAssetDescription());
                        tvBookQt.setText(assetDetails.getBookQty());
                        tvCostCenter.setText(assetDetails.getCostCenter());
                        tvDateCapitalization.setText(assetDetails.getCapDate());
                        tvGvalue.setText(assetDetails.getGrossBlock());
                        tvResponsibledepartment.setText(assetDetails.getResponsible_department());
                        tvLocation.setText(assetDetails.getLocation());
                        tvSubLocation.setText(assetDetails.getSub_location());


                    }
                }
            });


        } else {
            assetView.setVisibility(View.GONE);
            condition.setVisibility(View.VISIBLE);
            seleCtcodntitle.setVisibility(View.VISIBLE);
            batchDetail = SetCurrentBatch.getInstance().getCurrentBatch();

            String other_batch="";
            if (isOtherBatch){

                other_batch +="1";
            }else {
                other_batch+="0";
            }

            mViewModel.getScanResult(batchDetail.getProjectId(), batchDetail.getBatchId(), code, other_batch, verfy,"qrcode").observe(this, new Observer<DetailScan>() {
                @Override
                public void onChanged(DetailScan detailScan) {

                    Toast.makeText(mDetailsViewActivity, "" + detailScan.getRESPONSEMSG(), Toast.LENGTH_SHORT).show();
                    assetDetails = detailScan.getAssetDetails();
                    Dialog dialog = null;
                    if (detailScan.getRESPONSESTATUS().equals("3")) {
                        if (showPOP) {
                            showPOP = false;
                            isOtherBatch=false;
                            String[] dates = detailScan.getRESPONSEMSG().split(" ");
                            lastScanStatus.setText(dates[(dates.length - 1)]);
                            dialog = showCodeEnterDialog(detailScan.getRESPONSEMSG(),true);
                        }

                    }else if(detailScan.getRESPONSESTATUS().equals("2")){

                        if (showPOP) {
                            showPOP = false;
                            isOtherBatch=true;

                            dialog = showCodeEnterDialog(detailScan.getRESPONSEMSG(),false);
                        }

                    }


                    if (assetDetails != null) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        assetNo.setText("" + assetDetails.getAssetNumber());
                        tvAssetClassDescription.setText("" + assetDetails.getAssetClass());
                        tvassetDescription.setText(assetDetails.getAssetDescription());
                        tvBookQt.setText(assetDetails.getBookQty());
                        tvCostCenter.setText(assetDetails.getCostCenter());
                        tvDateCapitalization.setText(assetDetails.getCapDate());
                        tvGvalue.setText(assetDetails.getGrossBlock());

                        tvResponsibledepartment.setText(assetDetails.getResponsible_department());
                        tvLocation.setText(assetDetails.getLocation());
                        tvSubLocation.setText(assetDetails.getSub_location());



                    }
                }
            });
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);


    }

    private void SubmmitAssetApi() {


        if (assetDetails != null) {
            String text = (String) condition.getSelectedItem();


            if (text.equals("In Use")) {
                text = "";
                text = text + "yes";
            } else if (text.equals("-Select Condition-")) {
                text = "";
            } else {
                text = "";
                text = text + "no";
            }


            String use = isFromAsset ? inUse.getEditableText().toString() : "";
            String notInuse = isFromAsset ? notInUse.getEditableText().toString() : "";
            String notF = isFromAsset ? notFound.getEditableText().toString() : "";
            int post = SplReson.getSelectedItemPosition();

            String rgId = "";
            if (SplReson.getSelectedItem().equals("-Select Reason-")) {
                rgId = "";
            } else {
                if (post != 0) {
                    Reson.ReasonDetail reasonDetail = detailsReson.get(post-1);
                    rgId = reasonDetail.getReasonId();
                }
                
            }

            String mode = "";
            if (isFromAsset || isManulScan) {
                mode = mode + "manual";
            } else {
                mode = mode + "auto";
            }
            if ((isFromAsset == false) && (isManulScan == false) && text.isEmpty() && text.equals("")) {


                Toast.makeText(mDetailsViewActivity, "Please Select condition", Toast.LENGTH_SHORT).show();
                return;
            }

            int BookQT = Integer.parseInt(assetDetails.getBookQty());
            int useint = 0, notuse = 0, notFint = 0;
            if (!use.isEmpty()) {

                useint = Integer.parseInt(use);
            }
            if (!notInuse.isEmpty()) {

                notuse = Integer.parseInt(notInuse);
            }
            if ((!notF.isEmpty())) {
                notFint = Integer.parseInt(notF);
            }
            if (isManulScan || isFromAsset) {
                int total = useint + notuse + notFint;
                if (BookQT > total) {
                    Toast.makeText(mDetailsViewActivity, " total of use,not in use and not found must be greater than book Quantity.", Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            if (mode.equals("auto")) {

                if (text.isEmpty()) {
                    Toast.makeText(mDetailsViewActivity, "Select condition", Toast.LENGTH_SHORT).show();
                    return;
                }
            }


            mViewModel.SubmitAsset(assetDetails.getAssetId(), batchDetail.getProjectId(), mode, "1", "", batchDetail.getBatchId(), mUser.getProfileDetails().getUserId(),
                    rgId, remark.getText().toString(), use, notInuse, notF).observe(this, new Observer<SendRespose>() {
                @Override
                public void onChanged(SendRespose sendRespose) {
                    Toast.makeText(mDetailsViewActivity, "" + sendRespose.getRESPONSEMSG(), Toast.LENGTH_SHORT).show();
                    finish();

                }
            });
        } else {
            Toast.makeText(mDetailsViewActivity, "item not found ReScan", Toast.LENGTH_SHORT).show();
        }

    }

    public Dialog showCodeEnterDialog(String text,boolean isOverwrite) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.lay_custome_enter_manualcode);


        TextView test = dialog.findViewById(R.id.textmsg);
        test.setText(text);

        Button dialogButton = dialog.findViewById(R.id.mSubmit);
        Button Skip = dialog.findViewById(R.id.mSkip);

        if (!isOverwrite){
            dialogButton.setText("Yes");
            Skip.setText("No");
        }
        Skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mDetailsViewActivity.finish();
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDetails("1");
                dialog.dismiss();
            }
        });

        dialog.show();
        return dialog;

    }
}

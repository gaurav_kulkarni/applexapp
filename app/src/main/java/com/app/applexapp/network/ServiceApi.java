package com.app.applexapp.network;

import com.app.applexapp.model.AboutUs;
import com.app.applexapp.model.AssetList;
import com.app.applexapp.model.BatchDetails;
import com.app.applexapp.model.Dashboard;
import com.app.applexapp.model.DetailScan;
import com.app.applexapp.model.Profile;
import com.app.applexapp.model.Reson;
import com.app.applexapp.model.SendRespose;
import com.app.applexapp.model.SplashScreen;
import com.app.applexapp.model.SubmitBatch;
import com.app.applexapp.model.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServiceApi {

    @GET("app_get_splashscreen.php")
    Call<SplashScreen> getSplashScreen();

    @GET("app_get_aboutus.php")
    Call<AboutUs> getAboutUs();

    @FormUrlEncoded
    @POST("app_login.php")
    Call<User> UserLogin(
            @Field("username") String username,
            @Field("password") String password);

    @GET("app_get_batch.php")
    Call<BatchDetails> getBatchdetails(@Query("user_id") String user_id);

    @GET("app_get_dashboard_details.php")
    Call<Dashboard> getDashBoard(
            @Query("batch_id") String batchId,
            @Query("project_id") String ProjectId);

    @GET("app_get_assets.php")
    Call<AssetList> getAssetList(
            @Query("batch_id") String batchId,
            @Query("project_id") String ProjectId);

    @GET("app_verify_asset.php")
    Call<DetailScan> getScanResult(@Query("project_id") String ProjectId,
                                   @Query("batch_id") String batchId,
                                   @Query("qr_code") String qr_code,
                                   @Query("other_batch") String other_batch,
                                   @Query("re_verify") String re_verify,@Query("scan_by") String scantype);

    @GET("app_get_asset_details.php")
    Call<DetailScan> getAssetDetail(
            @Query("batch_id") String batch_id,
            @Query("project_id") String project_id,
            @Query("unique_id") String unique_id,
            @Query("other_batch") String other_batch,
            @Query("re_verify") String re_verify,
            @Query("reason") String reason,
            @Query("asset_id") String asset_id);

    @GET("app_get_reason.php")
    Call<Reson> getAllResons();

    @FormUrlEncoded
    @POST("app_submit_details.php")
    Call<SendRespose> SubmmitAsset(
                                   @Field("asset_id") String asset_id,
                                   @Field("project_id") String project_id,
                                   @Field("mode") String mode,
                                   @Field("qty") String qty,
                                   @Field("in_use") String in_use,
                                   @Field("batch_id") String batch_id,
                                   @Field("user_id") String user_id,
                                   @Field("reason_id") String reason_id,
                                   @Field("remark") String remark,
                                   @Field("in_use_qty") String in_use_qty,
                                   @Field("not_in_use_qty") String not_in_use_qty,
                                   @Field("not_found_qty") String not_found_qty);

    @FormUrlEncoded
    @POST("app_submit_batch.php")
    Call<SubmitBatch> submitBatch(@Field("asset_id") String batchid,
                                  @Field("project_id") String project_id,@Field("user_id") String user_id);

    @GET("app_get_profile.php")
    Call<Profile> getProfile(@Query("user_id") String id);

    /**
     *
     * http://applexinfotech.com/fav/api/app_submit_details.php?asset_id=101&project_id=4&mode=auto&qty=1&in_use=yes&batch_id=9&user_id=4&reason_id=&remark=&in_use_qty=&not_in_use_qty=&not_found_qty=

    /**
     *
     *
     *   http://applexinfotech.com/fav/api/app_verify_asset.php?project_id=1&batch_id=4&qr_code=7%201100%20VW14000036-0-0&other_batch=0&re_verify=0
     *
     */

}



package com.app.applexapp.fragment;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.applexapp.model.DetailScan;
import com.app.applexapp.model.Reson;
import com.app.applexapp.model.SendRespose;
import com.app.applexapp.network.RetrofitClient;
import com.app.applexapp.network.ServiceApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsViewModel extends ViewModel {

    private MutableLiveData<DetailScan> mBatchDetailsMutableLiveData = new MutableLiveData<DetailScan>();
    private MutableLiveData<Reson> mResons = new MutableLiveData<>();
    private MutableLiveData<SendRespose> mSendResponse = new MutableLiveData<>();
    private MutableLiveData<DetailScan> mBatmDeatilResult = new MutableLiveData<DetailScan>();

    public LiveData<DetailScan> getScanResult(String ProjectId,
                                              String batchId,
                                              String qr_code,
                                              String other_batch,
                                              String re_verify,String scan_by) {
        if (mBatchDetailsMutableLiveData != null) {
            loadScanResult(ProjectId, batchId, qr_code, other_batch, re_verify,scan_by);
        }
        return mBatchDetailsMutableLiveData;
    }

    public LiveData<SendRespose> SubmitAsset(String asset_id,
                                             String project_id,
                                             String mode,
                                             String qty,
                                             String in_use,
                                             String batch_id,
                                             String user_id,
                                             String reason_id,
                                             String remark,
                                             String in_use_qty,
                                             String not_in_use_qty,
                                             String not_found_qty) {
        if (mSendResponse != null) {

            SubmitAssetApi( asset_id,
                    project_id,
                    mode,
                    qty,
                    in_use,
                    batch_id,
                    user_id,
                    reason_id,
                    remark,
                    in_use_qty,
                    not_in_use_qty,
                    not_found_qty);
        }
        return mSendResponse;
    }

    private void loadScanResult(String ProjectId,
                                String batchId,
                                String qr_code,
                                String other_batch,
                                String re_verify,String scan_by) {
        ServiceApi serviceApi = RetrofitClient.getClient().create(ServiceApi.class);

        final Call<DetailScan> aboutUs = serviceApi.getScanResult(ProjectId, batchId, qr_code, other_batch, re_verify,scan_by);
        aboutUs.enqueue(new Callback<DetailScan>() {
            @Override
            public void onResponse(Call<DetailScan> call, Response<DetailScan> response) {
                DetailScan batchDetails = response.body();

                if (batchDetails != null) {
                    mBatchDetailsMutableLiveData.setValue(batchDetails);
                }
            }

            @Override
            public void onFailure(Call<DetailScan> call, Throwable t) {


            }
        });
    }

    public LiveData<Reson> getAllResons() {
        if (mResons != null) {
            loadResons();
        }
        return mResons;
    }

    private void loadResons() {
        ServiceApi serviceApi = RetrofitClient.getClient().create(ServiceApi.class);
        final Call<Reson> resons = serviceApi.getAllResons();
        resons.enqueue(new Callback<Reson>() {
            @Override
            public void onResponse(Call<Reson> call, Response<Reson> response) {
                Reson re = response.body();
                if (re != null) {
                    mResons.setValue(re);
                }
            }

            @Override
            public void onFailure(Call<Reson> call, Throwable t) {

            }
        });
    }

    private void SubmitAssetApi(String asset_id,
                                String project_id,
                                String mode,
                                String qty,
                                String in_use,
                                String batch_id,
                                String user_id,
                                String reason_id,
                                String remark,
                                String in_use_qty,
                                String not_in_use_qty,
                                String not_found_qty) {

        ServiceApi serviceApi = RetrofitClient.getClient().create(ServiceApi.class);
        final  Call<SendRespose> sendResposeCall=serviceApi.SubmmitAsset( asset_id,
                 project_id,
                 mode,
                 qty,
                 in_use,
                 batch_id,
                 user_id,
                 reason_id,
                 remark,
                 in_use_qty,
                 not_in_use_qty,
                 not_found_qty);
        sendResposeCall.enqueue(new Callback<SendRespose>() {
            @Override
            public void onResponse(Call<SendRespose> call, Response<SendRespose> response) {
               SendRespose sendRespose= response.body();
               if (sendRespose!=null){
                   mSendResponse.setValue(sendRespose);
               }
            }

            @Override
            public void onFailure(Call<SendRespose> call, Throwable t) {

            }
        });

    }

    public LiveData<DetailScan> getAssetDetails(String batch_id,
                                                String project_id,
                                                String unique_id,
                                                String other_batch,
                                                String re_verify,
                                                String reason,
                                                String asset_id) {
        if (mBatmDeatilResult != null) {
            loadScanResult(batch_id,
                    project_id,
                    unique_id,
                    other_batch,
                    re_verify,
                    reason,
                    asset_id);
        }
        return mBatmDeatilResult;
    }

    private void loadScanResult(String batch_id,
                                String project_id,
                                String unique_id,
                                String other_batch,
                                String re_verify,
                                String reason,
                                String asset_id) {
        ServiceApi serviceApi = RetrofitClient.getClient().create(ServiceApi.class);

        final Call<DetailScan> aboutUs = serviceApi.getAssetDetail(batch_id,
                project_id,
                unique_id,
                other_batch,
                re_verify,
                reason,
                asset_id);
        aboutUs.enqueue(new Callback<DetailScan>() {
            @Override
            public void onResponse(Call<DetailScan> call, Response<DetailScan> response) {
                DetailScan batchDetails = response.body();

                if (batchDetails != null) {
                    mBatmDeatilResult.setValue(batchDetails);
                }
            }

            @Override
            public void onFailure(Call<DetailScan> call, Throwable t) {


            }
        });
    }
}

package com.app.applexapp.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.applexapp.R;
import com.app.applexapp.model.AboutUs;
import com.app.applexapp.model.AboutusDetails;
import com.app.applexapp.network.RetrofitClient;
import com.app.applexapp.network.ServiceApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutUsActivity extends AppCompatActivity {

    private static final String TAG = "AboutUsActivity";

    ServiceApi serviceApi;
    TextView company;
    TextView telephone;
    TextView email;
    TextView website;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        company = findViewById(R.id.tvCompany);
        telephone = findViewById(R.id.tvTelephone);
        email = findViewById(R.id.tvEmail);
        website = findViewById(R.id.tvWebsite);

        getSupportActionBar().setTitle(getResources().getString(R.string.about_us));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void getData() {

        serviceApi = RetrofitClient.getClient().create(ServiceApi.class);

        final Call<AboutUs> aboutUs = serviceApi.getAboutUs();
        aboutUs.enqueue(new Callback<AboutUs>() {

            @Override
            public void onResponse(Call<AboutUs> call, Response<AboutUs> response) {

                AboutUs aboutUs = response.body();
                if (aboutUs != null) {
                    AboutusDetails aboutUsDetails = aboutUs.getAboutusDetails();
                    if (aboutUsDetails != null) {

                        Log.d(TAG, "onResponse: " + aboutUsDetails.getCompanyName());
                        company.setText(aboutUsDetails.getCompanyName());
                        telephone.setText(aboutUsDetails.getCompanyPhone());
                        email.setText(aboutUsDetails.getCompanyEmail());
                        website.setText(aboutUsDetails.getCompanyDomain());
                    }
                }
            }

            @Override
            public void onFailure(Call<AboutUs> call, Throwable t) {

                Toast.makeText(AboutUsActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                aboutUs.cancel();
            }
        });
    }
}
